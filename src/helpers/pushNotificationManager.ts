import * as Sentry from "@sentry/node";
import { admin } from "../config/firebaseInit";
import firebase from "firebase-admin/lib/messaging/index";

interface Payload {
  notification: {
    title: string;
    body: string;
    sound: string;
  };
  data: firebase.messaging.DataMessagePayload;
}

const sendPushNotification = (
  userId: string,
  title = "TiffinFinds",
  subTitle: string,
  data: firebase.messaging.DataMessagePayload
): void => {
  let payload: Payload = {
    notification: {
      title: title,
      body: subTitle,
      sound: "default",
    },
    data,
  };

  console.log("---- sendPushNotification --- CHANNEL --- ", userId);
  console.log(userId);
  admin
    .messaging()
    .sendToTopic("" + userId, payload)
    .then((response: any) => {
      console.log("---- PUSH NOTIFICATION SENT ----");
      console.log(response);
    })
    .catch((error: Error) => {
      Sentry.captureException(error);
      console.log("---- PUSH NOTIFICATION SENDING ERROR ----");
      console.log(error);
    });
};

export { sendPushNotification };
