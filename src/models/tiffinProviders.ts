import mongoose from "mongoose";
const Schema = mongoose.Schema;
import { LocationSchema } from "./location";

const TiffinProvidersSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: "users",
    },
    providerName: {
      type: String,
      required: true,
    },
    typeOfProvider: {
      type: String,
      required: true,
    },
    contactName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      unique: true,
      index: true,
      required: true,
    },
    contactNumber: {
      type: Number,
      unique: true,
      index: true,
      required: true,
    },
    location: LocationSchema,
    isCertified: {
      type: Boolean,
      required: true,
    },
    isGoodStanding: {
      type: Boolean,
      required: true,
    },
    spiceLevelAvailable: {
      type: Boolean,
    },
    doDelivery: {
      type: Boolean,
      required: true,
    },
    deliveryCities: {
      type: Array,
    },
    deliveryFrequency: {
      type: Array,
      required: true,
    },
    allowPickup: {
      type: Boolean,
      required: true,
    },
    menuCustomizable: {
      type: Boolean,
    },
    deliveryTimes: {
      type: Array,
      required: true,
    },
    kitchenCapacity: {
      type: String,
    },
    additionalMessages: {
      type: String,
    },
    vendorAgreement: {
      type: Boolean,
      required: true,
    },
    images: {
      type: Array,
    },
  },
  { timestamps: { createdAt: true, updatedAt: true } }
);
export const TiffinProviders = mongoose.model(
  "tiffinproviders",
  TiffinProvidersSchema
);
