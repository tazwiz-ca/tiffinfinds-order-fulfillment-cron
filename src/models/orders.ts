import mongoose from "mongoose";

const Schema = mongoose.Schema;
import { LocationSchema } from "./location";

const schema = new Schema(
  {
    tiffinProvider: {
      type: Object,
      required: true,
    },
    orderId: {
      type: Number,
    },
    customer: {
      type: Schema.Types.ObjectId,
      ref: "users",
      required: true,
    },
    mealFrequency: {
      type: String,
      required: true,
    },
    noOfDays: {
      type: Number,
    },
    receipt: {
      type: Object,
    },
    deliverySelected: {
      type: Boolean,
      required: true,
    },
    deliveryLocation: LocationSchema,
    pickupLocation: LocationSchema,
    deliveryTime: {
      type: String,
      required: true,
    },
    menu: {
      type: Object,
    },
    providerAccepted: {
      type: Boolean,
      default: false,
    },
    providerRejected: {
      type: Boolean,
      default: false,
    },
    remainingDays: {
      type: Number,
    },
    mealInstructions: {
      type: String,
    },
    deliveryInstructions: {
      type: String,
    },
    quantity: {
      type: Number,
      default: 1,
      required: true,
    },
    isCancelled: {
      type: Boolean,
      default: false,
    },
    startDate: {
      type: Date,
      required: true,
    },
    orderAcceptedDate: {
      type: Date,
    },
    orderCancelledDate: {
      type: Date,
    },
    orderDeclinedDate: {
      type: Date,
    },
    orderCompletedDate: {
      type: Date,
    },
    orderPausedDate: {
      type: Date,
    },
    orderResumedDate: {
      type: Date,
    },
    completedDays: {
      type: Number,
      default: 0,
    },
    orderStatus: {
      type: String,
      required: true,
    },
    couponId: {
      type: String,
    },
    paymentIntentId: {
      type: String,
      required: true,
    },
    paymentStatus: {
      type: String,
    },
    isRated: {
      type: Boolean,
      default: false,
    },
    ratings: {
      type: Number,
    },
    feedback: {
      type: Array,
    },
  },

  { timestamps: { createdAt: true, updatedAt: true } }
);

const Orders = mongoose.model("orders", schema);

export { Orders };
