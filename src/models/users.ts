import mongoose from "mongoose";
import { LocationSchema } from "./location";

const Schema = mongoose.Schema;
interface User extends mongoose.Document {
  fullName: string;
  password: string;
  contactNumber: Number;
  countryCode: string;
  email: string;
  typeOfUser: string;
  otpToken: string;
  otp: number;
  verifyOtpExpires: Date;
  isPhoneNumberVerified: boolean;
  resetPasswordToken: string;
  resetPasswordExpires: Date;
  source: string;
  location: typeof LocationSchema;
  deliveryAddresses: typeof LocationSchema;
  stripeInfo: object;
  paymentInfo: object;
  credits: number;
  isDeleted: boolean;
  coupon: Array<any>;
  notifications: object;
}
const schema = new Schema(
  {
    fullName: {
      type: String,
      index: true,
      required: true,
    },
    password: {
      type: String,
    },
    contactNumber: {
      type: Number,
    },
    countryCode: {
      type: String,
      default: "+1",
    },
    email: {
      type: String,
      index: true,
      unique: true,
      required: true,
    },
    typeOfUser: {
      type: String,
      required: true,
      enum: ["customer", "tiffinProvider"],
    },
    otpToken: {
      type: String,
    },
    otp: {
      type: Number,
    },
    verifyOtpExpires: {
      type: Date,
    },
    isPhoneNumberVerified: {
      type: Boolean,
      default: false,
    },
    resetPasswordToken: {
      type: String,
    },
    resetPasswordExpires: {
      type: Date,
    },
    source: {
      type: String,
      enum: ["normal", "google", "facebook", "apple"],
      default: "normal",
    },
    location: {
      type: [LocationSchema],
    },
    deliveryAddresses: {
      type: [LocationSchema],
    },
    stripeInfo: {
      customerId: {
        type: String,
      },
      cardInfo: [
        {
          isPrimary: {
            type: Boolean,
            default: false,
          },
          cardId: {
            type: String,
          },
        },
      ],
      connectedAccountId: {
        type: String,
      },
      requirements: {
        type: [String],
      },
      payoutsEnabled: {
        type: Boolean,
      },
      disabledReason: {
        type: String,
      },
      deadline: {
        type: Number,
      },
      verificationDetails: {
        type: String,
      },
      address: LocationSchema,
    },
    paymentInfo: {
      fullName: {
        type: String,
      },
      accountNumber: {
        type: String,
      },
      dobDay: {
        type: Number,
      },
      dobMonth: {
        type: Number,
      },
      dobYear: {
        type: Number,
      },
      address: LocationSchema,
    },
    credits: {
      type: Number,
      default: 0,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
    coupon: {
      type: Array,
    },
    notifications: {
      type: Object,
      default: {
        push: true,
        marketing: true,
      },
    },
  },
  { timestamps: { createdAt: true, updatedAt: true } }
);

const Users = mongoose.model<User>("users", schema);

export { Users };
