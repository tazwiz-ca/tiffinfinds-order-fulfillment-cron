import express from "express";
const app = express();
import * as dotenv from "dotenv";
dotenv.config();
import * as Sentry from "@sentry/node";
import mongoose from "mongoose";
import { processCron } from "./processor/cronProcessor";

//Initialize sentry for error logging
Sentry.init({
  dsn:
    "https://f126592690df46e28d048a3228a1de5b@o439611.ingest.sentry.io/5574557",
  tracesSampleRate: 1.0,
});

//Connect to db
mongoose
  .connect(process.env.MONGO_URI!, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Mongo DB Connected");
    processCron();
  })
  .catch((err) => Sentry.captureException(err));

app.listen(process.env.PORT || 5000);
