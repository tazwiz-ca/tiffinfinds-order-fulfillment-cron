import mongoose from "mongoose";
const Schema = mongoose.Schema;

export const GeoSchema = new Schema(
  {
    type: {
      type: String,
      default: "Point",
    },
    coordinates: {
      type: [Number],
    },
  },
  { _id: false }
).index({ coordinates: "2dsphere" });
