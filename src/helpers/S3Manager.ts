import AWS from "aws-sdk";
import moment from "moment";

const bucket = "tiffinfinds-cron-logs";
const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_ID,
  secretAccessKey: process.env.AWS_SECRET,
});

const s3_upload = (data: string): void => {
  const params = {
    Bucket: bucket,
    Key: `cron-${moment.now()}.log`,
    Body: data,
  };

  s3.upload(params, (err: Error) => {
    console.log("s3 upload error:: \n", err);
  });
};

export { s3_upload };
