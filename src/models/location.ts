import mongoose from "mongoose";
const Schema = mongoose.Schema;
import { GeoSchema } from "./geoSchema";

export const LocationSchema = new Schema({
  address: {
    type: String,
    trim: true,
  },
  locationId: {
    type: String,
  },
  city: {
    type: String,
    trim: true,
  },
  province: {
    type: String,
    trim: true,
  },
  postalCode: {
    type: String,
    trim: true,
  },
  coordinates: {
    type: GeoSchema,
  },
  fullAddress: {
    type: String,
    trim: true,
  },
  addressType: {
    type: String,
  },
  aptNo: {
    type: Number,
  },
  buzzerNo: {
    type: Number,
  },
  isDefault: {
    type: Boolean,
    default: false,
  },
});
