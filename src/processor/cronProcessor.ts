import cron from "node-cron";
import * as Sentry from "@sentry/node";
import mongoose from "mongoose";
import fetch from "node-fetch";
import moment from "moment-timezone";
import { Orders } from "../models/orders";
import { Users } from "../models/users";
import { s3_upload } from "../helpers/S3Manager";
import { sendPushNotification } from "../helpers/pushNotificationManager";

type OrderType = {
  _id: string;
  tiffinProviders: {
    providerName: string;
    images: [
      {
        s3Url: string;
      }
    ];
    deliveryFrequency: string[];
  };
  completedDays: number;
  noOfDays: number;
  tiffinId: string;
  providerName: string;
  orderId: number;
  customer: string;
};

type fullFillOrderType = {
  tiffProvImgUrl: string;
  tiffProvName: string;
  tiffinProviderId: string;
  id: string;
  orderId: number;
  userId: string;
};

export const processCron = () => {
  try {
    // Schedule tasks to be run on the server for fulfilling orders
    // 8:00 PM
    cron.schedule("0 19 * * *", async () => {
      const processedTime = moment().format("MMMM Do YYYY, h:mm:ss a");
      let dataToLog = "\n";

      dataToLog = dataToLog.concat(
        `====== Processing started at ${processedTime} ======= \n`
      );

      //Mark the completed days for the order before fulfilling the orders
      dataToLog = await markCompletedDays(dataToLog);
      //Fulfill the orders which are marked for completion for today
      dataToLog = await processOrdersForFulfillment(dataToLog);

      s3_upload(dataToLog);
    });

    // Schedule tasks to be run on the server for sending notification for orders completing soon
    // 12:30PM
    cron.schedule("30 11 * * *", async () => {
      const processedTime = moment().format("MMMM Do YYYY, h:mm:ss a");
      let dataToLog = "\n";

      dataToLog = dataToLog.concat(
        `====== Processing started at ${processedTime} ======= \n`
      );
      dataToLog = await sendResubscribeReminderNotification(dataToLog);

      s3_upload(dataToLog);
    });
  } catch (error) {
    Sentry.captureException(error);
  }
};

/**
 * Fetch all the active orders, scheduled for completion today
 * @returns
 */
const fetchOrdersForFulfillment = async () => {
  try {
    let fulfilledOrders: Array<fullFillOrderType> = [];
    // Fetch all the active orders
    let query = { orderStatus: "Active" };
    const activeOrders = await fetchActiveOrders(query);

    activeOrders.map((order) => {
      // Check if completed days and total no if days is equal
      if (order.completedDays == order.noOfDays) {
        fulfilledOrders.push({
          tiffProvImgUrl: order.tiffinProviders.images[0].s3Url,
          tiffProvName: order.tiffinProviders.providerName,
          tiffinProviderId: order.tiffinId,
          id: order._id,
          orderId: order.orderId,
          userId: order.customer,
        });
      }
    });
    return fulfilledOrders;
  } catch (error) {
    Sentry.captureException(error);
    return null;
  }
};

/**
 * At 12:30PM send a reminder for completing orders
 * "Your Tiffin Subscription is ending today, please re subscribe before 7 PM to receive re-start from tomorrow"
 * @param {*} dataToLog
 * @returns
 */
const sendResubscribeReminderNotification = async (
  dataToLog: string
): Promise<string> => {
  try {
    const fulfilledOrders = await fetchOrdersForFulfillment();
    if (!fulfilledOrders) {
      dataToLog = dataToLog.concat(
        `==== Something went wrong while fetching the fullFilled orders ====\n`
      );
      return dataToLog;
    }
    const orderIds = fulfilledOrders.map((order) => order.orderId);
    dataToLog = dataToLog.concat(
      `==== Processed orders are ${orderIds.join(",")} ====\n`
    );

    if (fulfilledOrders.length === 0) {
      dataToLog = dataToLog.concat(
        `==== No orders to send notification today!! ====\n`
      );
      return dataToLog;
    }

    fulfilledOrders.forEach((order) => {
      //Send push notification to user
      let data = {
        screen: "Order",
        orderId: order.orderId + "",
        orderStatus: "Active",
        order_Id: order.id + "",
      };

      const id = order.userId;
      sendPushNotification(
        id,
        "Your Tiffinfinds order status update",
        `Your order ${order.orderId} is ending today, please re subscribe before 7 PM to receive re-start from tomorrow`,
        data
      );
      dataToLog = dataToLog.concat(
        `==== Notification for ${order.orderId} sent!! ====\n`
      );
    });
  } catch (error) {
    console.log(error);
    dataToLog = dataToLog.concat(
      "Error in sendResubscribeReminderNotification() ==> " +
        error.message.toString()
    );
    Sentry.captureException(error);
  }
  return dataToLog;
};

/**
 * Query and fetch the active orders from DB
 * @param {*} dataToLog
 * @returns
 */
const fetchActiveOrders = async (query: object): Promise<Array<OrderType>> => {
  try {
    // Merge the Orders and TiffinProviders collection to check the actual endDate
    const activeOrders: Array<OrderType> = await Orders.aggregate([
      { $match: query },
      {
        $project: {
          tiffinId: { $toObjectId: "$tiffinProvider._id" },
          startDate: 1,
          customer: 1,
          noOfDays: 1,
          orderId: 1,
          completedDays: 1,
        },
      },
      {
        $lookup: {
          localField: "tiffinId",
          from: "tiffinproviders",
          foreignField: "_id",
          as: "tiffinProviders",
        },
      },
      { $unwind: "$tiffinProviders" },
    ]);
    return activeOrders;
  } catch (error) {
    console.log(error);
    Sentry.captureException(error);
    return [];
  }
};

/**
 * If today is part of the provider's delivery day for any active order
 * Then increment the completion day by 1
 * @param {*} dataToLog
 * @returns
 */
const markCompletedDays = async (dataToLog: string): Promise<string> => {
  try {
    type orderMarkCompletion = {
      _id: string;
      completedDays: number;
    };
    let ordersToMarkCompletion: Array<orderMarkCompletion> = [];
    // Fetch all the active orders
    let query = { orderStatus: "Active", startDate: { $lte: new Date() } };
    const activeOrders: Array<OrderType> = await fetchActiveOrders(query);

    activeOrders.map((order) => {
      //Check if today is part of the delivery day for the provider
      if (
        order.tiffinProviders.deliveryFrequency.includes(
          moment().format("dddd").toLowerCase()
        )
      ) {
        //Add all the orders which are to be marked for completed days to the array
        ordersToMarkCompletion.push({
          _id: order._id,
          completedDays: order.completedDays,
        });
      }
    });

    if (ordersToMarkCompletion.length > 0) {
      for (let order of ordersToMarkCompletion) {
        dataToLog = dataToLog.concat(
          `======== Order incrementing completed days ${order._id} ========\n`
        );
        await Orders.findByIdAndUpdate(mongoose.Types.ObjectId(order._id), {
          completedDays: order.completedDays
            ? parseInt(order.completedDays.toString()) + 1
            : 1,
        });
      }
    } else {
      dataToLog = dataToLog.concat(
        "No orders available to mark completed days today"
      );
    }
  } catch (error) {
    console.log(error);
    dataToLog = dataToLog.concat(
      "Error in markCompletedDays() ==> " + error.message.toString()
    );
    Sentry.captureException(error);
  }
  return dataToLog;
};

/**
 * Step-1: Fetch active orders
 * Step-2: Update the orders which are getting completed today
 * @param {*} dataToLog
 * @returns
 */
const processOrdersForFulfillment = async (
  dataToLog: string
): Promise<string> => {
  try {
    // fetch all the active orders, about to be completed today based on date
    const fulfilledOrders = await fetchOrdersForFulfillment();
    if (!fulfilledOrders) {
      dataToLog = dataToLog.concat(
        `==== Something went wrong while fetching the fullFilled orders ====\n`
      );
      return dataToLog;
    }
    // update the orders which are fulfilled
    dataToLog = await updatedOrderCompletion(dataToLog, fulfilledOrders);
  } catch (error) {
    console.log(error);
    dataToLog = dataToLog.concat(
      "Error in fetchOrdersForFulfillment() ==> " + error.message.toString()
    );
    Sentry.captureException(error);
  }
  return dataToLog;
};

/**
 * Update all the orders which are passed in the argument to "Completed" status
 * @param {*} dataToLog
 * @param {*} fulfilledOrders
 * @returns
 */
const updatedOrderCompletion = async (
  dataToLog: string,
  fulfilledOrders: Array<fullFillOrderType>
): Promise<string> => {
  try {
    if (fulfilledOrders.length > 0) {
      const orderIds = fulfilledOrders.map((order) => order.id);
      const orderDetails = fulfilledOrders.map((order) => ({
        id: order.id,
        orderId: order.orderId,
        userId: order.userId,
      }));
      dataToLog = dataToLog.concat(
        `==== Processed orders are ${orderIds.join(",")} ====\n`
      );
      const result = await Orders.updateMany(
        { _id: { $in: orderIds } },
        {
          $set: {
            orderStatus: "Completed",
            orderCompletedDate: moment().tz("America/Toronto").toDate(),
          },
        },
        { multi: true }
      );

      if (result) {
        dataToLog = dataToLog.concat(`==== Update is success=====\n`);
        for (const order of orderDetails) {
          try {
            dataToLog = dataToLog.concat(
              `==== Entering order status updation for monday.com ${order}, ${order.userId}=====\n`
            );

            try {
              //Call the internal api to payout the vendors for specific orders
              await fetch(
                process.env.API_URL + `payments/payoutVendor/${order.id}`,
                {
                  method: "POST",
                  headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                  },
                }
              );
            } catch (error) {
              dataToLog = dataToLog.concat(
                `==== Error occured while paying out vendor for order ${order.id} with error ==> ${error.message}=====\n`
              );
            }
            //Fetch the customer's email address
            const user = await Users.findById(order.userId);

            if (!user) {
              dataToLog = dataToLog.concat(
                `==== The user object is null=====\n`
              );
              return dataToLog;
            }
            dataToLog = dataToLog.concat(
              `==== The user email id is ${user.email}=====\n`
            );

            //Update the monday.com api board for the order status change
            await fetch(
              process.env.API_URL + "miscellaneous/updateMondayOrderStatus",
              {
                method: "PUT",
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json",
                },
                body: JSON.stringify({
                  email: user.email,
                  orderId: order.orderId,
                  orderStatus: "Completed",
                }),
              }
            );

            dataToLog = dataToLog.concat(
              `==== Successfully updated monday.com dashboard for order status =====\n`
            );
          } catch (error) {
            Sentry.captureException(error);
            dataToLog = dataToLog.concat(
              "Error while fetching user and updating the monday.com api board ==> " +
                error.message.toString()
            );
          }
        }

        fulfilledOrders.forEach((order) => {
          //Send push notification to user
          let completedData = {
            screen: "Order",
            order_Id: order.id + "",
            orderId: order.orderId + "",
            orderStatus: "Completed",
          };

          const id = order.userId;
          sendPushNotification(
            id,
            "Your Tiffinfinds order status update",
            `Your order ${order.orderId} is completed`,
            completedData
          );

          //Notify the users to review the tiffin provider
          let reviewData = {
            screen: "Review",
            tiffinProviderId: order.tiffinProviderId + "",
            tiffProvImgUrl: order.tiffProvImgUrl + "",
            tiffProvName: order.tiffProvName + "",
            order_Id: order.id + "",
          };
          sendPushNotification(
            id,
            "Your Tiffinfinds order status update",
            `Please review and rate your order ${order.orderId}`,
            reviewData
          );

          dataToLog = dataToLog.concat(
            `==== Notification for ${order.orderId} sent!! ====\n`
          );
        });
      } else
        dataToLog = dataToLog.concat(
          `==== Something went wrong while updating =====\n`
        );
    } else {
      dataToLog = dataToLog.concat(`==== No orders to process!!! =====\n`);
    }
  } catch (error) {
    console.log(error);
    dataToLog = dataToLog.concat(
      "Error in updatedOrderCompletion() ==> " + error.message.toString()
    );
    Sentry.captureException(error);
  }
  return dataToLog;
};
